import sys
import struct

is_py2 = sys.version[0] == '2'

from xmlrpc.server import SimpleXMLRPCServer

def getInput():
    f = open("/dev/input/js0", "rb")

    for i in range(0, 23):
        f.read(8)

    data = f.read(8)
    button = struct.unpack('8b', data)
    print(button)
    order = ""
    if button[7] == 0:
        order = "BACK"
    elif button[7] == 1:
        order = "OPEN"
    elif button[7] == 2:
        order = "FRONT"
    elif button[7] == 3:
        order = "CLOSE"
    elif button[7] == 4:
        order = "DOWN"
    elif button[7] == 5:
        order = "UP"
    elif button[7] == 15:
        order = "LEFT"
    elif button[7] == 16:
        order = "RIGHT"

    return order

server = SimpleXMLRPCServer(("", 50000), allow_none=True)
server.RequestHandlerClass.protocol_version = "HTTP/1.1"
print("Listening on port 50000...")
server.register_function(getInput, "getInput")
server.serve_forever()
